const CROSS = 'X';
const ZERO = 'O';
const EMPTY = ' ';

//Максимальное количесиво ходов
let MAX_MOVE_COUNT = 9;
//Двумерный массив представляющий игровое поле, может содержать 3 значения CROSS / ZERO / EMPTY
let GAMEFIELD;
//Размерность игрового поля
let FIELD_DIMENSION = 3;
//Символ предыдущего хода
let prevState = EMPTY;
//Счетчик количества ходов
let moveCounter = 0;
//Логическая переменная определяющая закончилась ли игра
let isGameEnd = false;
//Логическая переменная определяющая использование AI
let isAIMode = false;
//Логическая переменная определяющая пренадлежит ли ход AI
let isAIMoveOwn = false;

startGame();

function startGame() {
  inputFieldDimension();
  renderGrid(FIELD_DIMENSION);
  initGamefield(FIELD_DIMENSION);
  onAIMode();
}

function onAIMode() {
  isAIMode = confirm("Активировать AI?");
}

function inputFieldDimension() {

  while (true) {
    FIELD_DIMENSION = prompt('Какой размер поля x на x?', FIELD_DIMENSION);

    if (FIELD_DIMENSION == null) {
      FIELD_DIMENSION = 3;
      break;
    }

    if (Number.isNaN(FIELD_DIMENSION)) {
      alert('Размер поля это число!');
      continue;
    }

    if (FIELD_DIMENSION < 2 || FIELD_DIMENSION > 30) {
      alert('Размер поля должен быть больше 2 и меньше 30!');
      continue;
    }

    break;
  }
  MAX_MOVE_COUNT = FIELD_DIMENSION * FIELD_DIMENSION;
}

function initGamefield(demension) {
  let line = [];
  GAMEFIELD = [];

  for (let i = 0; i < demension; i++) {
    line.push(EMPTY);
  }

  for (let i = 0; i < demension; i++) {
    GAMEFIELD.push(line.slice());
  }
}

function resetParams() {
  moveCounter = 0;
  isGameEnd = false;
  prevState = EMPTY;
  //isAIMode = false;
  showMessage('');

}


function checkWin(lastMove) {

  /*Проверку начинаем когда прошло миниальное количество ходов необходимое для победы */
  if (moveCounter >= GAMEFIELD.length * 2 - 1) {
    checkLines(lastMove);

    if (moveCounter === MAX_MOVE_COUNT && !isGameEnd) {
      showMessage("Победила дружба");
      isGameEnd = true;
    }
  }
}

/**
 * Класс представляющий одну итерацию проверки ячейки в игровом поле
 */
class StepCheck {

  constructor(checkedSymbol, maxStepCount) {
    this.state = true;
    this.coordHistory = [];
    this.checkedSymbol = checkedSymbol;
    this.maxStepCount = maxStepCount;
    this.stepCount = 0;
  }
  /**
   * Метод представляет собой одну итерацию проверки значения игрового поля
   * @param {number} i Целое число от 0 до FIELD_DIMENSION
   * @param {number} j Целое число от 0 до FIELD_DIMENSION
   */
  check(i, j) {

    //true до тех пор пока 1 раз false
    this.state = this.state ? (GAMEFIELD[i][j] === this.checkedSymbol) : false;

    if (this.state)
      this.coordHistory.push([i, j]);

    if (this.maxStepCount - 1 === this.stepCount) {

      if (this.state) {
        this.colorizedWinSolution();
        showMessage(`Победил ${this.checkedSymbol} !`);
        isGameEnd = true;
      }
    }

    this.stepCount++

    return this.state;
  }

  reset() {
    this.coordHistory = [];
    this.state = true;
    this.stepCount = 0;
  }

  colorizedWinSolution() {
    for (let elem of this.coordHistory) {
      renderSymbolInCell(this.checkedSymbol, elem[0], elem[1], '#DC143C');
    }
  }
}


function checkLines(symbol) {
  const size = GAMEFIELD[0].length;
  let stepMainDiagonalCheck = new StepCheck(symbol, size);
  let stepSideDiagonalCheck = new StepCheck(symbol, size);
  let stepHorizontalCheck = new StepCheck(symbol, size);
  let stepVerticalCheck = new StepCheck(symbol, size);

  for (let i = 0; i < size; i++) {

    stepMainDiagonalCheck.check(i, i);
    stepSideDiagonalCheck.check((size - 1) - i, i)

    stepHorizontalCheck.reset();
    stepVerticalCheck.reset();

    for (let j = 0; j < size; j++) {
      stepHorizontalCheck.check(i, j);
      stepVerticalCheck.check(j, i)
    }

  }
}

function getNextState() {
  return (prevState === EMPTY || prevState === ZERO) ? CROSS : ZERO;
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function moveAI() {
  let x;
  let y;
  do {
    x = getRandomInt(FIELD_DIMENSION);
    y = getRandomInt(FIELD_DIMENSION);
  } while (GAMEFIELD[x][y] != EMPTY);

  console.log(`AI ходит на ${x},${y}`);
  cellClickHandler(x, y);
}

/* обработчик нажатия на клетку */
function cellClickHandler(row, col) {
  console.log(`Clicked on cell: ${row}, ${col}`);

  if (GAMEFIELD[row][col] === EMPTY && !isGameEnd) {

    isAIMoveOwn = !isAIMoveOwn;
    let symbol = getNextState();
    prevState = symbol;
    GAMEFIELD[row][col] = symbol;
    moveCounter++;
    renderSymbolInCell(symbol, row, col);
    if (moveCounter)
      checkWin(symbol);

    if (isAIMode && isAIMoveOwn)
      moveAI();
  }

}

/* обработчик нажатия на кнопку "Сначала" */
function resetClickHandler() {
  console.log('reset!');
  renderGrid(FIELD_DIMENSION);
  initGamefield(FIELD_DIMENSION);
  resetParams();
  showMessage('');
}

/* Служебные фукнции для взаимодействия с DOM. Данные функции нельзя редактировать! */
/* Показать сообщение */
function showMessage(text) {
  var msg = document.querySelector('.message');
  msg.innerText = text
}

/* Нарисовать игровое поле заданного размера */
function renderGrid(dimension) {
  var container = getContainer();
  container.innerHTML = '';

  for (let i = 0; i < dimension; i++) {
    var row = document.createElement('tr');
    for (let j = 0; j < dimension; j++) {
      var cell = document.createElement('td');
      cell.textContent = EMPTY;
      cell.addEventListener('click', () => cellClickHandler(i, j));
      row.appendChild(cell);
    }
    container.appendChild(row);
  }
}

/* Нарисовать символ symbol в ячейку(row, col) с цветом color */
function renderSymbolInCell(symbol, row, col, color = '#333') {
  var targetCell = findCell(row, col);

  targetCell.textContent = symbol;
  targetCell.style.color = color;
}

function findCell(row, col) {
  var container = getContainer();
  var targetRow = container.querySelectorAll('tr')[row];
  return targetRow.querySelectorAll('td')[col];
}

function getContainer() {
  return document.getElementById('fieldWrapper');
}

/* Test Function */
/* Победа первого игрока */
function testWin() {
  clickOnCell(0, 2);
  clickOnCell(0, 0);
  clickOnCell(2, 0);
  clickOnCell(1, 1);
  clickOnCell(2, 2);
  clickOnCell(1, 2);
  clickOnCell(2, 1);
}

/* Ничья */
function testDraw() {
  clickOnCell(2, 0);
  clickOnCell(1, 0);
  clickOnCell(1, 1);
  clickOnCell(0, 0);
  clickOnCell(1, 2);
  clickOnCell(1, 2);
  clickOnCell(0, 2);
  clickOnCell(0, 1);
  clickOnCell(2, 1);
  clickOnCell(2, 2);
}

function clickOnCell(row, col) {
  findCell(row, col).click();
}
